﻿using System;

namespace Kai.Universal.Data {
    public class ColumnInfo {
        public string ColName { get; set; }
        public string ModelName { get; set; }
        public Type ColType { get; set; }
        //public int ColLength { get; set; }
    }
}
