﻿namespace Kai.Universal.Text {

    public enum WordCase {
        LowerCamel,
        UpperCamel,
        LowerUnderscore,
        UpperUnderscore,
        LowerHyphen,
        UpperHyphen,
    }
}