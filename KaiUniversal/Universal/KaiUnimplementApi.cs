﻿using System;

namespace Kai.Universal {
    /// <summary>
    /// this is mean Kai has api, waiting for implemented
    /// </summary>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class KaiUnimplementApi : Attribute {
    }
}
