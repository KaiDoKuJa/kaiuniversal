﻿using System;

namespace Kai.Universal {
    /// <summary>
    /// this is mean other Kai has api, but noneed implemented here
    /// </summary>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class KaiDummyApi : Attribute {
    }
}
