﻿namespace Kai.Universal.Sql.Where {
    public class Replacement {
        public string Value { get; set; }
        public string ReplacePattern { get; set; }
    }
}
