﻿namespace Kai.Universal.Sql.Result {
    public enum SqlGeneratorMode {

        Statement,
        PreparedStatement
    }
}
