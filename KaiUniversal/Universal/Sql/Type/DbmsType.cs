﻿namespace Kai.Universal.Sql.Type {
    public enum DbmsType {
        Default,
        FromSqlServer2005,
        FromSqlServer2012,
    }
}
