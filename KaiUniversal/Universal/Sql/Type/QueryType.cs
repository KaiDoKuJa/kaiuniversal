﻿namespace Kai.Universal.Sql.Type {

    public enum QueryType {
        Select,
        SelectAll,
        SelectPaging,
        SelectTop,
        SelectCnt,
    }

}
