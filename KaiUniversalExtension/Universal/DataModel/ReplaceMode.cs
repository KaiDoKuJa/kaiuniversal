﻿namespace Kai.Universal.DataModel {
    public enum ReplaceMode {
        Default = 0,

        Before = 1,
        After = 2
    }
}
